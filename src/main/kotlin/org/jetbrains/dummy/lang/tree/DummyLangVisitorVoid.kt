package org.jetbrains.dummy.lang.tree

abstract class DummyLangVisitorVoid<in D> : DummyLangVisitor<Unit, D>() {

    override fun visitElement(element: Element, data: D) {}

    override fun visitFile(file: File, data: D) {
        file.acceptChildren(this, data)
    }

    override fun visitFunctionDeclaration(functionDeclaration: FunctionDeclaration, data: D) {
        functionDeclaration.acceptChildren(this, data)
    }

    override fun visitBlock(block: Block, data: D) {
        block.acceptChildren(this, data)
    }

    override fun visitStatement(statement: Statement, data: D) {
        statement.acceptChildren(this, data)
    }

    override fun visitAssignment(assignment: Assignment, data: D) {
        assignment.acceptChildren(this, data)
    }

    override fun visitIfStatement(ifStatement: IfStatement, data: D) {
        ifStatement.acceptChildren(this, data)
    }

    override fun visitVariableDeclaration(variableDeclaration: VariableDeclaration, data: D) {
        variableDeclaration.acceptChildren(this, data)
    }

    override fun visitReturnStatement(returnStatement: ReturnStatement, data: D) {
        returnStatement.acceptChildren(this, data)
    }

    override fun visitExpression(expression: Expression, data: D) {
        expression.acceptChildren(this, data)
    }

    override fun visitVariableAccess(variableAccess: VariableAccess, data: D) {
        variableAccess.acceptChildren(this, data)
    }

    override fun visitIntConst(intConst: IntConst, data: D) {
        intConst.acceptChildren(this, data)
    }

    override fun visitBooleanConst(booleanConst: BooleanConst, data: D) {
        booleanConst.acceptChildren(this, data)
    }

    override fun visitFunctionCall(functionCall: FunctionCall, data: D) {
        functionCall.acceptChildren(this, data)
    }
}