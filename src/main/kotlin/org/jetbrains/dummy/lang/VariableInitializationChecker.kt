package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.tree.*

class VariableInitializationChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {

    override fun inspect(file: File) {
        file.acceptChildren(VariableInitializationCheckerVisitor(), AnalysisFramework())
    }

    private inner class VariableInitializationCheckerVisitor : DummyLangVisitorVoid<AnalysisFramework>() {

        override fun visitFunctionDeclaration(functionDeclaration: FunctionDeclaration, data: AnalysisFramework) {
            functionDeclaration.parameters.forEach { data.addDeclared(it) }
            super.visitFunctionDeclaration(functionDeclaration, data)
        }

        override fun visitVariableDeclaration(variableDeclaration: VariableDeclaration, data: AnalysisFramework) {
            super.visitVariableDeclaration(variableDeclaration, data)
            data.addDeclared(variableDeclaration.name, variableDeclaration.initializer != null)
        }

        override fun visitAssignment(assignment: Assignment, data: AnalysisFramework) {
            super.visitAssignment(assignment, data)
            data.addExisting(assignment.variable)
        }

        override fun visitIfStatement(ifStatement: IfStatement, data: AnalysisFramework) {
            ifStatement.condition.accept(this, data)
            data.joinBranches(
                { ifStatement.thenBlock.accept(this, data) },
                { ifStatement.elseBlock?.accept(this, data) }
            )
        }

        override fun visitVariableAccess(variableAccess: VariableAccess, data: AnalysisFramework) {
            if (variableAccess.name !in data) {
                reportAccessBeforeInitialization(variableAccess)
            }
            super.visitVariableAccess(variableAccess, data)
        }
    }

    private class AnalysisFramework {
        private var currentScope: AnalysisScope = AnalysisScope(mutableSetOf(), mutableListOf())

        fun addDeclared(variableIdentifier: String, initialized: Boolean = true) {
            currentScope.addDeclared(variableIdentifier, initialized)
        }

        fun addExisting(variableIdentifier: String) {
            currentScope.addExisting(variableIdentifier)
        }

        operator fun contains(variableIdentifier: String) = variableIdentifier in currentScope

        fun joinBranches(vararg branchBlocks: () -> Unit) {
            for (nextBranch in branchBlocks) {
                scoped { nextBranch() }
            }
            currentScope.consumeChildren()
        }

        fun scoped(block: () -> Unit) {
            val newNode = AnalysisScope(mutableSetOf(), mutableListOf(), parent = currentScope)
            currentScope.children += newNode
            currentScope = newNode
            block()
            currentScope = currentScope.parent!!
        }

        private class AnalysisScope(
            val infos: MutableSet<VariableInfo>,
            val children: MutableList<AnalysisScope>,
            val parent: AnalysisScope? = null
        ) {
            fun addDeclared(variableIdentifier: String, initialized: Boolean = true) {
                infos += VariableInfo(variableIdentifier, this, initialized)
            }

            fun addExisting(variableIdentifier: String) {
                find(variableIdentifier)?.let { variableInfo ->
                    infos += VariableInfo(variableIdentifier, variableInfo.declarationScope, initialized = true)
                }
            }

            // true here is a little crutch to not fall into error when
            // variable was not declared.
            operator fun contains(variableIdentifier: String) = find(variableIdentifier)?.initialized ?: true

            fun consumeChildren() {
                infos += getChildrenResultsIntersection()
                children.clear()
            }

            private fun find(variableIdentifier: String): VariableInfo? =
                infos.findLast { it.identifier == variableIdentifier }
                    ?: parent?.find(variableIdentifier)

            private fun getChildrenResultsIntersection(): Set<VariableInfo> {
                if (children.isEmpty()) {
                    return emptySet()
                }
                return children
                    .map { it.infos.toSet() }
                    .reduce { acc, nextChildInfo -> acc.intersect(nextChildInfo) }
            }
        }

        private class VariableInfo(
            val identifier: String,
            val declarationScope: AnalysisScope,
            val initialized: Boolean
        ) {
            override fun equals(other: Any?): Boolean = when (other) {
                is VariableInfo -> identifier == other.identifier
                        && declarationScope === other.declarationScope
                        && initialized && other.initialized
                else -> false
            }

            override fun hashCode(): Int {
                var result = identifier.hashCode()
                result = 31 * result + declarationScope.hashCode()
                result = 31 * result + initialized.hashCode()
                return result
            }

        }
    }

    // Use this method for reporting errors
    private fun reportAccessBeforeInitialization(access: VariableAccess) {
        reporter.report(access, "Variable '${access.name}' is accessed before initialization")
    }
}