package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.tree.*

class RedeclarationChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {

    override fun inspect(file: File) {
        class VariablesDeclaredInBlock(val declaredVariables: MutableSet<String> = mutableSetOf())

        file.acceptChildren(object : DummyLangVisitorVoid<Nothing?>() {
            private val nestedBlocks = mutableListOf<VariablesDeclaredInBlock>()
            private fun currentBlock() = nestedBlocks.last()

            override fun visitBlock(block: Block, data: Nothing?) {
                nestedBlocks += VariablesDeclaredInBlock()
                super.visitBlock(block, data)
                nestedBlocks.removeAt(nestedBlocks.lastIndex)
            }

            override fun visitVariableDeclaration(variableDeclaration: VariableDeclaration, data: Nothing?) {
                if (variableDeclaration.name in currentBlock().declaredVariables) {
                    reportVariableRedeclaration(variableDeclaration)
                }
                super.visitVariableDeclaration(variableDeclaration, data)
                currentBlock().declaredVariables += variableDeclaration.name
            }
        }, data = null)
    }

    private fun reportVariableRedeclaration(declaration: VariableDeclaration) {
        reporter.report(declaration, "Redeclaration of variable '${declaration.name}'")
    }
}