package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.tree.*

class UndeclaredVariableChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {

    override fun inspect(file: File) {
        class VariablesDeclaredInBlock(val declaredVariables: MutableSet<String> = mutableSetOf())

        file.acceptChildren(object : DummyLangVisitorVoid<Nothing?>() {
            private val nestedBlocks = mutableListOf<VariablesDeclaredInBlock>()
            private fun currentBlock() = nestedBlocks.last()

            override fun visitFunctionDeclaration(functionDeclaration: FunctionDeclaration, data: Nothing?) {
                nestedBlocks += VariablesDeclaredInBlock()
                functionDeclaration.parameters.forEach { currentBlock().declaredVariables += it }
                super.visitFunctionDeclaration(functionDeclaration, data)
                nestedBlocks.removeAt(nestedBlocks.lastIndex)
            }

            override fun visitBlock(block: Block, data: Nothing?) {
                nestedBlocks += VariablesDeclaredInBlock()
                super.visitBlock(block, data)
                nestedBlocks.removeAt(nestedBlocks.lastIndex)
            }

            override fun visitVariableDeclaration(variableDeclaration: VariableDeclaration, data: Nothing?) {
                super.visitVariableDeclaration(variableDeclaration, data)
                currentBlock().declaredVariables += variableDeclaration.name
            }

            override fun visitVariableAccess(variableAccess: VariableAccess, data: Nothing?) {
                if (!isDeclared(variableAccess.name)) {
                    reportUseOfUndeclaredVariable(variableAccess)
                }
                super.visitVariableAccess(variableAccess, data)
            }

            override fun visitAssignment(assignment: Assignment, data: Nothing?) {
                if (!isDeclared(assignment.variable)) {
                    reportUseOfUndeclaredVariable(assignment)
                }
                super.visitAssignment(assignment, data)
            }

            private fun isDeclared(variableIdentifier: String): Boolean =
                nestedBlocks.any { variableIdentifier in it.declaredVariables }

        }, data = null)
    }

    private fun reportUseOfUndeclaredVariable(variableAccess: VariableAccess) {
        reporter.report(variableAccess, "Access to undeclared variable '${variableAccess.name}'")
    }

    private fun reportUseOfUndeclaredVariable(assignment: Assignment) {
        reporter.report(assignment, "Assignment to undeclared variable '${assignment.variable}'")
    }
}