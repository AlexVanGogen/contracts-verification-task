package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.tree.*

class ReferencesTable {
    private val assignments = mutableMapOf<Assignment, VariableDeclaration>()
    private val accesses = mutableMapOf<VariableAccess, VariableDeclaration>()

    operator fun get(assignment: Assignment) = assignments[assignment]
    operator fun set(assignment: Assignment, declaration: VariableDeclaration) {
        assignments[assignment] = declaration
    }

    operator fun get(access: VariableAccess) = accesses[access]
    operator fun set(access: VariableAccess, declaration: VariableDeclaration) {
        accesses[access] = declaration
    }
}

fun Element.collectReferences(): ReferencesTable {
    val table = ReferencesTable()
    accept(ReferencesCollector(), table)
    return table
}

class ReferencesCollector : DummyLangVisitorVoid<ReferencesTable>() {
    private val nestedBlocks = mutableListOf<VariablesDeclaredInBlock>()

    private fun currentBlock() = nestedBlocks.last()

    override fun visitBlock(block: Block, data: ReferencesTable) {
        nestedBlocks += VariablesDeclaredInBlock()
        super.visitBlock(block, data)
        nestedBlocks.removeAt(nestedBlocks.lastIndex)
    }

    override fun visitVariableDeclaration(variableDeclaration: VariableDeclaration, data: ReferencesTable) {
        super.visitVariableDeclaration(variableDeclaration, data)
        currentBlock().declaredVariables += variableDeclaration.name to variableDeclaration
    }

    override fun visitAssignment(assignment: Assignment, data: ReferencesTable) {
        findDeclaration(assignment.variable)?.let { data[assignment] = it }
        super.visitAssignment(assignment, data)
    }

    override fun visitVariableAccess(variableAccess: VariableAccess, data: ReferencesTable) {
        findDeclaration(variableAccess.name)?.let { data[variableAccess] = it }
        super.visitVariableAccess(variableAccess, data)
    }

    private fun findDeclaration(variableIdentifier: String) =
        nestedBlocks.findLast { variableIdentifier in it.declaredVariables }
            ?.declaredVariables
            ?.entries
            ?.first { (identifier, _) -> variableIdentifier == identifier }
            ?.value

    class VariablesDeclaredInBlock(val declaredVariables: MutableMap<String, VariableDeclaration> = mutableMapOf())
}