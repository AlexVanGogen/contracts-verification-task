package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.tree.DummyLangVisitorVoid
import org.jetbrains.dummy.lang.tree.File
import org.jetbrains.dummy.lang.tree.FunctionCall
import org.jetbrains.dummy.lang.tree.FunctionDeclaration

class FunctionsChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {

    override fun inspect(file: File) {
        file.acceptChildren(DuplicateFunctionsVisitor(), mutableSetOf())
        file.acceptChildren(IncorrectCallsVisitor(file.functions), data = null)
        file.acceptChildren(DuplicateFunctionParametersVisitor(), data = null)
    }

    private inner class DuplicateFunctionsVisitor : DummyLangVisitorVoid<MutableSet<FunctionDeclaration>>() {
        override fun visitFunctionDeclaration(
            functionDeclaration: FunctionDeclaration,
            data: MutableSet<FunctionDeclaration>
        ) {
            data.find { it.sameAs(functionDeclaration) }?.let {
                reportDuplicateFunction(functionDeclaration)
            }
            data += functionDeclaration
            super.visitFunctionDeclaration(functionDeclaration, data)
        }

        private fun FunctionDeclaration.sameAs(other: FunctionDeclaration): Boolean {
            return name == other.name && parameters.size == other.parameters.size
        }
    }

    private inner class IncorrectCallsVisitor(val functions: List<FunctionDeclaration>) : DummyLangVisitorVoid<Nothing?>() {
        override fun visitFunctionCall(functionCall: FunctionCall, data: Nothing?) {
            if (functions.none { functionCall.sameAs(it) }) {
                reportCalledFunctionDoesNotExist(functionCall)
            }
            super.visitFunctionCall(functionCall, data)
        }

        private fun FunctionCall.sameAs(other: FunctionDeclaration): Boolean {
            return function == other.name && arguments.size == other.parameters.size
        }
    }

    private inner class DuplicateFunctionParametersVisitor : DummyLangVisitorVoid<Nothing?>() {
        override fun visitFunctionDeclaration(functionDeclaration: FunctionDeclaration, data: Nothing?) {
            val parametersOccurrenceCounts = functionDeclaration.parameters
                .groupingBy { it }
                .eachCount()
            val parametersDeclaredMoreThanOnce = parametersOccurrenceCounts.filterValues { it > 1 }.keys
            if (parametersDeclaredMoreThanOnce.isNotEmpty()) {
                reportDuplicateFunctionParameters(functionDeclaration, parametersDeclaredMoreThanOnce)
            }
        }
    }

    private fun reportDuplicateFunction(function: FunctionDeclaration) {
        reporter.report(function, "Function '${function.name}' with ${function.parameters.size} parameter(s) already exists")
    }

    private fun reportCalledFunctionDoesNotExist(call: FunctionCall) {
        reporter.report(call, "Function '${call.function}' with ${call.arguments.size} argument(s) does not exist")
    }

    private fun reportDuplicateFunctionParameters(function: FunctionDeclaration, parameters: Collection<String>)  {
        reporter.report(function, "Function '${function.name}' has parameter(s) ${parameters.joinToString { "'$it'" }} declared more than once")
    }
}