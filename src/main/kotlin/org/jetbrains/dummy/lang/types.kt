package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.tree.*

fun File.collectVariableTypes(references: ReferencesTable): VariableTypesTable {
    val types = VariableTypesTable(references)
    acceptChildren(TypesCollector(references), types)
    return types
}

fun Expression.getType(
    types: VariableTypesTable
): Type {
    val sink = TypeSink(AnyType)
    accept(ExpressionTypeExtractor(types), sink)
    return sink.type
}

class ExpressionTypeExtractor(private val types: VariableTypesTable) : DummyLangVisitorVoid<TypeSink>() {
    override fun visitVariableAccess(variableAccess: VariableAccess, data: TypeSink) {
        data.type = types[variableAccess] ?: AnyType
    }

    override fun visitIntConst(intConst: IntConst, data: TypeSink) {
        data.type = IntType
    }

    override fun visitBooleanConst(booleanConst: BooleanConst, data: TypeSink) {
        data.type = BooleanType
    }

    override fun visitFunctionCall(functionCall: FunctionCall, data: TypeSink) {
        data.type = AnyType
    }
}

class TypeSink(var type: Type)

class TypesCollector(val references: ReferencesTable) : DummyLangVisitorVoid<VariableTypesTable>() {
    override fun visitVariableDeclaration(variableDeclaration: VariableDeclaration, data: VariableTypesTable) {
        variableDeclaration.initializer?.let { data[variableDeclaration] = it.getType(data) }
        super.visitVariableDeclaration(variableDeclaration, data)
    }

    override fun visitAssignment(assignment: Assignment, data: VariableTypesTable) {
        references[assignment]?.let { variableDeclaration ->
            val assignmentType = assignment.rhs.getType(data)
            data.putIfAbsent(variableDeclaration, assignmentType)
        }
        super.visitAssignment(assignment, data)
    }
}

class VariableTypesTable(val references: ReferencesTable) {
    private val types = mutableMapOf<VariableDeclaration, Type>()

    operator fun get(declaration: VariableDeclaration) = types[declaration]
    operator fun set(declaration: VariableDeclaration, type: Type) {
        types[declaration] = type
    }
    operator fun contains(declaration: VariableDeclaration) = declaration in types

    operator fun get(assignment: Assignment) = references[assignment]?.let { types[it] }
    operator fun get(variableAccess: VariableAccess) = references[variableAccess]?.let { types[it] }

    fun putIfAbsent(declaration: VariableDeclaration, type: Type) {
        types.putIfAbsent(declaration, type)
    }
}

sealed class Type {
    abstract fun render(): String
}

object IntType : Type() {
    override fun render() = "Integer"
}

object BooleanType : Type() {
    override fun render() = "Boolean"
}

object AnyType : Type() {
    override fun render() = "Any"
}