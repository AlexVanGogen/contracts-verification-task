package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.tree.*

class IntraproceduralTypeChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {
    override fun inspect(file: File) {
        val referencesTable = file.collectReferences()
        val typesTable = file.collectVariableTypes(referencesTable)
        file.acceptChildren(IntraproceduralTypeCheckerVisitor(typesTable), data = null)
    }

    private inner class IntraproceduralTypeCheckerVisitor(
        val types: VariableTypesTable
    ) : DummyLangVisitorVoid<Nothing?>() {

        override fun visitAssignment(assignment: Assignment, data: Nothing?) {
            types[assignment]?.let { existingVariableType ->
                val assignmentType = assignment.rhs.getType(types)
                if (assignmentType != existingVariableType && assignmentType != AnyType) {
                    reportTypeMismatch(assignment.rhs, existingVariableType, assignmentType)
                }
            }

            super.visitAssignment(assignment, data)
        }

        override fun visitIfStatement(ifStatement: IfStatement, data: Nothing?) {
            val conditionExpressionType = ifStatement.condition.getType(types)
            if (conditionExpressionType != BooleanType && conditionExpressionType != AnyType) {
                reportConditionTypeMismatch(ifStatement.condition, conditionExpressionType)
            }
            super.visitIfStatement(ifStatement, data)
        }
    }

    private fun reportTypeMismatch(expression: Expression, expected: Type, actual: Type) {
        reporter.report(expression, "Type mismatch: expected ${expected.render()}, found ${actual.render()}")
    }

    private fun reportConditionTypeMismatch(expression: Expression, actual: Type) {
        reportTypeMismatch(expression, BooleanType, actual)
    }
}