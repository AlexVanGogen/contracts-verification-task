package org.jetbrains.dummy.lang

import org.junit.Test

class DummyLanguageTestGenerated : AbstractDummyLanguageTest() {
    @Test
    fun testAssignToItselfBad() {
        doTest("testData/assignToItselfBad.dummy")
    }
    
    @Test
    fun testAssignToItselfGood() {
        doTest("testData/assignToItselfGood.dummy")
    }
    
    @Test
    fun testBad() {
        doTest("testData/bad.dummy")
    }
    
    @Test
    fun testCallToNonexisingFunction() {
        doTest("testData/callToNonexisingFunction.dummy")
    }
    
    @Test
    fun testComplicatedBooleanInCondition() {
        doTest("testData/complicatedBooleanInCondition.dummy")
    }
    
    @Test
    fun testComplicatedBooleanOrIntegerInCondition() {
        doTest("testData/complicatedBooleanOrIntegerInCondition.dummy")
    }
    
    @Test
    fun testDistressedBooleanInCondition() {
        doTest("testData/distressedBooleanInCondition.dummy")
    }
    
    @Test
    fun testDuplicateFunctionParameters() {
        doTest("testData/duplicateFunctionParameters.dummy")
    }
    
    @Test
    fun testDuplicatedFunctions() {
        doTest("testData/duplicatedFunctions.dummy")
    }
    
    @Test
    fun testFunParametersGood() {
        doTest("testData/funParametersGood.dummy")
    }
    
    @Test
    fun testFunctionCallInCondition() {
        doTest("testData/functionCallInCondition.dummy")
    }
    
    @Test
    fun testFunctionParameterAsCondition() {
        doTest("testData/functionParameterAsCondition.dummy")
    }
    
    @Test
    fun testFunctionsScopeBad() {
        doTest("testData/functionsScopeBad.dummy")
    }
    
    @Test
    fun testGood() {
        doTest("testData/good.dummy")
    }
    
    @Test
    fun testIfThenElseBad1() {
        doTest("testData/ifThenElseBad1.dummy")
    }
    
    @Test
    fun testIfThenElseBad2() {
        doTest("testData/ifThenElseBad2.dummy")
    }
    
    @Test
    fun testIfThenElseBad3() {
        doTest("testData/ifThenElseBad3.dummy")
    }
    
    @Test
    fun testIfThenElseGood1() {
        doTest("testData/ifThenElseGood1.dummy")
    }
    
    @Test
    fun testIfThenElseGood2() {
        doTest("testData/ifThenElseGood2.dummy")
    }
    
    @Test
    fun testIfThenElseGood3() {
        doTest("testData/ifThenElseGood3.dummy")
    }
    
    @Test
    fun testIntLiteralInCondition() {
        doTest("testData/intLiteralInCondition.dummy")
    }
    
    @Test
    fun testIntVariableInCondition() {
        doTest("testData/intVariableInCondition.dummy")
    }
    
    @Test
    fun testRedeclarationBad() {
        doTest("testData/redeclarationBad.dummy")
    }
    
    @Test
    fun testRedeclarationInTheSameScope() {
        doTest("testData/redeclarationInTheSameScope.dummy")
    }
    
    @Test
    fun testUndeclaredVars() {
        doTest("testData/undeclaredVars.dummy")
    }
}
